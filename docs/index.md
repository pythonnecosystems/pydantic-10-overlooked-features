# Pydantic의 간과하기 쉬운 10가지 기능 <sup>[1](#footnote_1)</sup>


<a name="footnote_1">1</a>: 이 페이지는 [Pydantic: 10 Overlooked Features You Should Be Using](https://medium.com/@kasperjuunge/pydantic-10-overlooked-features-you-should-be-using-8f0bac05c60c)을 편역한 것임.
