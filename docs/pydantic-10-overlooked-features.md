# Pydantic의 간과하기 쉬운 10가지 기능 <sup>[1](#footnote_1)</sup>
Python용 데이터 유효성 검사와 설정 관리 라이브러리인 Pydantic은 사용 편의성과 강력한 기능으로 빠르게 인기를 얻고 있다. 하지만 그 잠재력을 최대한 활용하고 있는가? 잘 알려지지 않았지만 매우 강력한 몇 가지 기능을 소개한다.

## 기능 1: 필드 커스터마이제이션
필드 클래스를 사용하여 모델 필드를 커스터마이징한다. 이렇게 하면 명확성이 향상될 뿐만 아니라 제약 조건과 기본값을 도입한다.

```python
from pydantic import BaseModel, Field

class Item(BaseModel):
    name: str = Field(default="Unknown", max_length=100, description="Name of the item")
```

## 기능 2: `pre`와 `post`로 유효성 검사기
유효성 검사 전에 데이터를 클린싱하거나 유효성 검사 후에 수정해야 하는 경우가 있다. `pre`과 `post` 유효성 검사기를 입력할 수 있다.

```python
from pydantic import BaseModel, validator

class TextModel(BaseModel):

    text: str

    @validator('text', pre=True)
    def sanitize_string(cls, value):
        return value.strip()

    @validator('text', post=True)
    def to_uppercase(cls, value):
        return value.upper()
```

## 기능 3: 루트 유효성 검사기
이를 통해 전체 모델에 대한 유효성 검사를 수행할 수 있어 필드 간 일관성 검사에 유용하다.

```python
from pydantic import BaseModel, root_validator

class Coordinate(BaseModel):

    x: float
    y: float

    @root_validator
    def check_coordinates(cls, values):
        x, y = values.get('x'), values.get('y')
        if x == y:
            raise ValueError('x and y cannot be the same.')
        return values
```

## 기능 4: 재귀 모델
스레드 댓글과 같은 시나리오에 적합한, 스스로를 참조하는 모델을 디자인하세요.

```python
from typing import List

class Comment(BaseModel):
    text: str
    replies: List['Comment'] = []

Comment.update_forward_refs()
```

## 기능 5: 동적 모델 생성
코드를 작성할 때 모델 구조를 모를 때가 있다. Pydantic이 도와줄 수 있다.

```python
from pydantic import create_model

fields = {'name': (str, ...), 'age': (int, ...)}
DynamicUser = create_model('DynamicUser', **fields)
```


## 기능 6: 알리아스
API에서 Python이 아닌 필드 이름이 들어오고 있나? 알리아스를 사용하여 Python 코드를 관용적으로 유지하세요.

```python
from pydantic import BaseModel

class User(BaseModel):
    username: str = Field(alias='user-name')
```

## 기능 7: 제네릭 모델
유연한 유형 정의가 필요할 때 사용합니다.

```python
from pydantic.generics import GenericModel
from typing import TypeVar

T = TypeVar('T')

class Wrapper(GenericModel, Generic[T]):
    content: T
```

## 기능 8: 설정 관리
Pydantic을 사용하여 환경 변수, 구성 파일 등으로부터 앱 구성을 관리할 수 있다.

```python
from pydantic import BaseSettings

class AppConfig(BaseSettings):

    app_name: str = "MyApp"
    debug: bool = False

    class Config:
        env_file = ".env"
```

## 기능 9: JSON 직렬화 커스터마이제이션
Pydantic을 사용하면 필요에 맞게 JSON 직렬화 프로세스를 커스터마이징할 수 있다.

```python
from pydantic import BaseModel, Field

class Fruit(BaseModel):

    name: str
    taste: str = Field(alias='flavor')

    class Config:
        json_encoders = {str: lambda v: v.upper()}
```

## 기능 10: 계산된 필드
수동 개입 없이 다른 필드에서 모델 필드를 동적으로 계산할 수 있다.

```python
from pydantic import BaseModel

class FullName(BaseModel):

    first_name: str
    last_name: str

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"
```


<a name="footnote_1">1</a>: 이 페이지는 [Pydantic: 10 Overlooked Features You Should Be Using](https://medium.com/@kasperjuunge/pydantic-10-overlooked-features-you-should-be-using-8f0bac05c60c)을 편역한 것임.